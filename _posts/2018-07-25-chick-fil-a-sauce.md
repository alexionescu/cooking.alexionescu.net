---
title: Chick-Fil-A Sauce
categories: 
  - Sauce
date: 2018-07-25
featured_image: https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/1bca24c47a32244afc4bdc9c665f3456f40cbaf175d76ddf6b8ea769db0afea8.jpg
recipe:
  servings: 8
  prep: 5 minutes
  cook: 1 minute
  ingredients_markdown: |-
    **Sauce**

    * 1/2 cup mayo
    * 2 tsp mustard
    * 1 tsp lemon juice
    * 2 tbs honey
    * 1 tbs smokey BBQ sauce
  directions_markdown: |-
    **Sauce**

    1. Whisk all ingredients together.
---
This sauce is really tangy and is one of my favorite sauces to pair along
with chicken. It's also really easy to make and you most likely have all
the ingredients already in your kitchen.

![Chick-Fil-A Sauce](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/d15dbf924bcb9ce6e1657f7d082f1a7d36f50ac3102d796ef497336be08e0d6a.jpg)

Doesn't look appetizing before mixing the everything together, but don't worry!

![Chick-Fil-A Sauce](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/1bca24c47a32244afc4bdc9c665f3456f40cbaf175d76ddf6b8ea769db0afea8.jpg)

After the ingredients have been mixed together. Yum!
