---
title: New York Style Pizza
categories: 
  - Pizza
date: 2018-07-25
featured_image: https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/00239b8e24d60812285fa10c99d4c0b97d3033168fab6cd26d5b55150e981799.jpg
recipe:
  servings: 3 12" Pizzas
  prep: 60 minutes
  cook: 10 minutes
  ingredients_markdown: |-
    **New York Style Pizza**

    * [Thin Crust Pizza Dough](/pizza/2018/07/25/thin-crust-pizza-dough/)
    * [Pizza Sauce](/pizza/sauce/2018/07/25/pizza-sauce) or whatever other sauce you like
    * Flour to create a non-stick surface
    * Mozzarella Cheese
    * Any other topings you want!
  directions_markdown: |-
    **New York Style Pizza**

    1. 1 hour before you wish to bake the pizzas begin to preheat the oven as hot as it will go.
       If you have one of those fancy ovens that can reach 550 F, great! Brick oven pizza ovens
       normally reach 800+ F! If you don't have a baking steel or stone, you can use an upside
       down baking tray to have a hot conductive surface to cook your pizza on. 
    2. In addition, to preheating the oven, take out the dough from the fridge so it has time to
       warm up, so you can shape it.
    3. Flour a wooden cutting board.
    4. Begin by stretching out the dough into approximately 12" circles. Keep moving it on the
       cutting board or else it would stick!
    5. Place a very thin layer of tomato sauce on the pizza. Just enough so that when you partially
       bake it in the oven it will not bubble up! Do not make the mistake I did or you will have
       big bubbles in your crust!
    6. Use a pizza peel to transfer to your oven. Bake for 3-4 minutes at 500 F. If your oven is
       hotter than this, adjust accordingly.
    7. Remove the pizza and place additional sauce.
    8. Add mozzarella cheese and any other additional toppings you prefer!
    9. Place back into the oven and cook to desired doneness. I prefer a darker crust which takes
       approximately 8 minutes at 500 F. An 800 F oven can achieve this in 90 seconds.
---
So you've made the pizza dough, and pizza sauce and now it's time to assemble everything together!

![New York Style Pizza](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/78e58f097014ff03ed7dc8d9f5e874b1f448b273685bf9b0a84d40f9af8a7bdf.jpg)

Flouring a wooden cutting board.

![New York Style Pizza](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/0a752d77c7e6c1bfa3818c530e4fc748aa6a86f8cd00c86665715c5272f3251f.jpg)

Partially baking the pizza dough. I forgot to add some pizza sauce to hold the bubbles down!

![New York Style Pizza](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/d0595f9676f79d6d563d9a537dcd83db02196d262ba78a93a2bae16e8ff86f34.jpg)

Just a couple of minutes in the oven and it should start developing some brown spots. If you want,
you can freeze this dough for later use.

![New York Style Pizza](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/3bb4cf3e3f9725c3ea60355aa3b1494ca18d9af4bab9dcc40560fcd9aab51547.jpg)

Adding the tomato sauce.

![New York Style Pizza](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/6aef619746721e5ad84bda1417650bd53c625ef041f87513441ac572a102f0b9.jpg)

I add the meat first, as I find it burns quite easily if it is laid above the cheese.

![New York Style Pizza](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/bba79d20b76d40273f47d8656fa136fb0e0ee01fa2006cc5c24f3861a0ce37a7.jpg)

Mmm, cheese.

![New York Style Pizza](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/00239b8e24d60812285fa10c99d4c0b97d3033168fab6cd26d5b55150e981799.jpg)

All out of the oven!

![New York Style Pizza](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/c2d846a889b3e413d4a8d37c09cee7c8136087738dc9db719b63ef00c746439e.jpg)

This is about how dark I prefer it. Give it a try, I think it comes out a lot better when there are
dark spots.
