---
title: Pizza Sauce
categories: 
  - Pizza
  - Sauce
date: 2018-07-25
featured_image: https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/635bd18184f63e01f7d7a98210e9a6d7464aa49f488960293537f09ad685f7dc.jpg
recipe:
  servings: 800 g (3 cups), enough for 4-5 12" pizzas
  prep: 15 minutes
  cook: 40 minutes
  ingredients_markdown: |-
    **Basic Sauce**

    * 1 can (800 g/28 oz) Whole Peeled or Crushed Tomates
    * 20 g extra-virgin olive oil
    * 1 clove garlic
    * 8 g fine sea salt.
    * 0.3g (1/4 tsp) dried oregano
    * 0.4 g (1/4 tsp) chili flakes 

    **Vodka Sauce**

    * 750 g Basic Sauce (above)
    * 75 g vodka
    * 50 g heavy cream
    * 5 g grated Grana Padano or Parmigiano-Reggiano cheese
  directions_markdown: |-
    **Basic Sauce**

    1. Put the olive oil, garlic, salt, oregano, and chili flakes in a blender. Add just
       a spoonful of tomatoes and blend briefly. Then, add the rest of the tomatoes. Brief
       pulse until the ingredients are combined. Overblending can lead to a more watery
       sauce as the tomatoes will release moisture.
    2. Pour the sauce into a sealable container. It can keep for 1 week in the fridge.

    **Vodka Sauce**
    1. In a saucepan over medium-high heat reduce the vodka until about 2 tablespoons remain,
       about 5 minutes.
    2. Pour in the cream and cook gently over low heat, stirring a few times, for 1 or 2 minutes.
    3. Add the sauce and cheese. Raise the heat to high for a few minutes just, until it starts 
       to boil. Reduce the heat to medium-low and simmer uncovered, for 30 minutes.
    4. Set the sauce aside to cool, then pour it into a sealable container. It can keep for 1
       week in the fridge.

---
This sauce goes great on pizza, spaghetti, etc. You can make the basic version which has ingredients
you most likely already have in your pantry, or the fancy version for a more creamier flavor.

![Pizza Sauce](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/e79361b44554cfd65be8fc919438ebf6b60bc22da86a5cc247c07487109034d5.jpg)

These are the tomatoes I use from Costco. While they are not San Marzano, you can't beat 6 pounds
of sauce for $2.99.

![Pizza Sauce](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/7590e97afbaa1317f8fa2de415646322c61bd3b68237770dcf7fbbf25c2d63b2.jpg)

Spice and olive oil blend.

![Pizza Sauce](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/ea7e6bd20c1668d7d4123cb3df258086b9ad306695aa665ddcad694b6ddae373.jpg)

Basic sauce after pulsing in the blender and combined with the spice and olive oil blend.

![Pizza Sauce](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/28a92819e16f3b446a16866b60fa0728e44a6867519a2b9eccb662dc88b9b329.jpg)

Reduced vodka, mixed with heavy cream.

![Pizza Sauce](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/4e22cd5458aef198d8cc5cdafe79b37e05b0c1513916ddb5d446090c778a9257.jpg)

Grated cheese added to sauce. I like cheese, so I ended up adding more than the recipe calls for.
