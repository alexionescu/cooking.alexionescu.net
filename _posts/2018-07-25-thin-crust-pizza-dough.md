---
title: Thin Crust Pizza Dough
categories: 
  - Pizza
date: 2018-07-25
featured_image: https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/618490d589f41b2455d94f7cdc075b1ebb3329d85de24d0a28887641dbba6b61.jpg
recipe:
  servings: 3 12" Pizzas
  prep: 10 minutes
  cook: 24-72 hours
  ingredients_markdown: |-
    **Dough**

    * 320 g Water (90 F)
    * 14 g Salt
    * 1.2 g (approx 0.4 tsp) Instant Dried Yeast
    * 500 g Bread Flour
  directions_markdown: |-
    **Dough**

    1. Measure and combine all ingredients.
    2. Mix the dough with a wooden spoon or with a dough hook for 90 seconds on the slowest speed.
       The final target dough temperature is 78 to 80 F. Put in a covered tub for a 2 hour rise
       at room temperature.
    3. Divide dough into 3 balls, approximately 270 g each.
    4. Shrink wrap the dough balls, or place covered into separate containers in the fridge.
    5. Dough will be ready to use in 24-72 hours. Best at 48 hours.
---
This thin crust pizza dough recipe is my go to. It consists of just 4 simple ingredients: flour,
water, salt, and yeast. While you do have to plan ahead, the cold fermentation is what gives
this dough a complex flavor profile. We could have chosen to use more yeast to have the dough
be ready in the same day, however your patience will be rewarded.

![Thin Crust Pizza Dough](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/3047cc380c1bf7850abf25f935b84f5126c271b0068d45951a2e01647fefc797.jpg)

Combining all the dry ingredients together.

![Thin Crust Pizza Dough](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/bb4e0bb0e42ee78b880a75f48bfd2ff2085f26f4865690dbd1c0552b91bb7a9f.jpg)

Adding the water into the mix.

![Thin Crust Pizza Dough](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/44be0789a69ccb2dc46c559b807fec383b2f956d89dd3c85dadd1e677b732ea2.jpg)

After starting to mix the dough, you may think it is a bit dry, however keep incorporating all
of the flour into the mixture.

![Thin Crust Pizza Dough](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/122e6524b0e45deaa4706d0b6b4e48faa250cb9c5c0614ae12a37572785189ae.jpg)

Covering the dough to ferment at room temperature for 2 hours.

![Thin Crust Pizza Dough](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/618490d589f41b2455d94f7cdc075b1ebb3329d85de24d0a28887641dbba6b61.jpg)

3 dough balls of approximately 270 g each to be left in the fridge for the cold ferment.
