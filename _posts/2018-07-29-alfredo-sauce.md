---
title: Alfredo Sauce
categories: 
  - Sauce
date: 2018-07-29
featured_image: https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/4e7c92e145a47f6bb3e58e6cd304e6d8fffbdbaa8121d2ed72c9425a5236ed45.jpg
recipe:
  servings: For 1 lb of pasta
  prep: 10 minutes
  cook: 10 minutes
  ingredients_markdown: |-
    **Sauce**

    * 1/4 cup (4 tbs) butter
    * 1 cup heavy cream
    * 1 clove garlic, finely minced
    * 1 1/2 cups freshly grated Parmesean

    **Herbs**
    * 1 tbs Italian seasoning (as pictured)
    * Alternatively, 1 tsp basil, thyme, and oregano each
    * Alternatively, 1/4 cup chopped fresh parsley
  directions_markdown: |-
    **Sauce**

    1. Melt butter in a medium saucepan over medium low heat.
    2. Add cream and simmer for 5 minutes.
    3. Add garlic and cheese, and whisk quickly, heating thorough.
    4. Stir in herbs and add to pasta. Remember to reserve some pasta water so that sauce is not too thick.
---
My go to Alfredo sauce. 

![Alfredo Sauce](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/4af840565cf92c7327e1e1cc0b3d1a437d1bc657255df0d7e668a7ed7483e5c2.jpg)

Adding heavy cream into the melted butter.

![Alfredo Sauce](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/910b3a593c77b70dc03e850dc3ee2e9a5874765321ad77ab98855409faa4dc6b.jpg)

Garlic and cheese added!

![Alfredo Sauce](https://cooking-cdn.alexionescu.net/file/cooking-cdn-alexionescu/4e7c92e145a47f6bb3e58e6cd304e6d8fffbdbaa8121d2ed72c9425a5236ed45.jpg)

Finished product!
